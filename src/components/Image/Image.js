import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object, //id
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.state = {
      size: 200,
      rotation: 0,
      like: false
      };
    this.calcImageSize = this.calcImageSize.bind(this);
    this.handleRotate = this.handleRotate.bind(this);
    this.handleShare = this.handleShare.bind(this);
    this.handleLike = this.handleLike.bind(this);
  }

  calcImageSize() {
    const width = document.body.clientWidth
    //const {galleryWidth} = this.props;
    const targetSize = 150;
    let imagesPerRow = Math.round(width / targetSize);
    //let remainder = (width % targetSize);
    const size = ((width / imagesPerRow));
    this.setState({size});
  }

  componentDidMount() {
    this.calcImageSize();
    window.addEventListener('resize', this.calcImageSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calcImageSize);
  }

  static urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }
//----------------------Handle Events:-----------------------------------------
  handleRotate() {
    let newRotation = this.state.rotation + 90;
    if(newRotation >= 360){
      newRotation =- 360;
    }
    this.setState({
      rotation: newRotation
    })
  }

  handleShare() {
    const url = Image.urlFromDto(this.props.dto);
    const text = 'Image';
    const image = Image.urlFromDto(this.props.dto);
    open('http://facebook.com/sharer.php?s=100&p[url]=' + url +
      '&p[images][0]=' + image + '&p[title]=' + text, 'fbshare',
      'height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0');
  }

  handleLike(){
    this.setState({like: !this.state.like})
  }
//------------------------------------------------------------------------------------

  render() {
    const { rotation } =  this.state;
    //boolean check for Like button:
    let like_icon = this.state.like ? 'yesLike' : 'noLike';
    let like_icon_corner = this.state.like ? 'yesLikeCorner' : 'noLikeCorner';
    let imageRoot = this.state.like ? 'image-root-pink' : 'image-root-white';
    return (
      <div
    className={imageRoot}
    style={{
      backgroundImage: `url(${Image.urlFromDto(this.props.dto)})`,
        width: this.state.size + 'px',
        height: this.state.size + 'px',
        transform: `rotate(${rotation}deg)`
    }}
  >
  <FontAwesome className={like_icon_corner} name='heart' title='Like' onClick={this.handleLike} />

    <div style={{ transform: `rotate(${-rotation}deg)`}}>
      <FontAwesome className="image-icon" name="trash-alt" title="Delete" onClick={() => this.props.onDelete(this.props.dto.id)}  />
      <FontAwesome className={like_icon} name="heart" title="Like" onClick={this.handleLike} />
      <FontAwesome className="image-icon" name="sync-alt" title="Rotate" onClick={this.handleRotate} />
      <FontAwesome className="expand-icon" name="expand" title="Expand" onClick={() => this.props.onExpand(this.props.index)} />
      <FontAwesome className="share-icon" name="paper-plane" title="Share" onClick={this.handleShare} />
    </div>
      </div>
  );
  }
}

export default Image;







