import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import Lightbox from 'react-images';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      imagesUrl: [],
      page: 1,
      galleryWidth: this.getGalleryWidth(),
      currentImage: 0,
    };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    // Binds the scroll event handler:
    window.onscroll = () => {
      const {
        getImages,
      } = this;
      // Checks that the page has scrolled to the bottom:
      if (
        window.innerHeight + document.documentElement.scrollTop
        >= (document.documentElement.offsetHeight - 10)
      ) {
        this.getImages(this.props.tag);
      }
    };
  }

//----------------Lightbox Functions: ---------------------------

  openLightbox(indx) {
    console.log(indx);
    this.setState({
      currentImage: indx,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }

 //--------------------------------------------------------

  handleDelete = (imageId) => {
    const images = this.state.images.filter(c => c.id !== imageId);
    this.setState({images});
  }

  getGalleryWidth() {
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(tag) {
    var page = this.state.page;
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&format=json&safe_search=1&nojsoncallback=1&page=${page}&extras=url_o`;
    const baseUrl = 'https://api.flickr.com/';
    this.setState({page: page + 1});
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET',
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          //concat the newly loaded photos to the images array:
          const currentImages= this.state.images;
          const displayImg = currentImages.concat(res.photos.photo);
          this.setState({images: displayImg});
        }
      });
      }

  componentDidMount() {
    this.getImages(this.props.tag);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillReceiveProps(props) {
    this.getImages(props.tag);
  }

  render() {
    const {
      imagesUrl,
      images,
    } = this.state;
    return (
      <div>
        <div className="gallery-root">
          {this.state.images.map((dto,index) => {
            return <Image key={'image-' + dto.id} dto={dto} index={index} galleryWidth={this.state.galleryWidth} onDelete={this.handleDelete} onExpand={this.openLightbox}/>;
          })}
          <Lightbox images={this.state.imagesUrl = this.state.images.map((img)=> ({src: Image.urlFromDto(img)}))}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
            backdropClosesModal={true}
            /> </div>
      </div>
    );
  }
}

export default Gallery;








